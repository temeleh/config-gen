#!/usr/bin/env python3
import argparse
import shellinterpolation
import configparser
import os
from pathlib import Path
from generator import Generator


DEFAULT_SECTION = "COMMON"
META_SECTION = "META"


def get_main_section_proxy(mapping):
    main_section = mapping[META_SECTION].get('main-section', DEFAULT_SECTION)

    if not mapping.has_section(main_section):
        print(f'Invalid main-section "{main_section}" defined! Using section "{DEFAULT_SECTION}"')
        main_section = DEFAULT_SECTION

    return mapping[main_section]


def is_valid_path(path, is_file=False):
    is_valid = path.is_file() if is_file else path.is_dir()
    if not is_valid:
        print(f"Invalid {'file' if is_file else 'directory'} {path}")
    return is_valid


def load_mapping(mappingfile):
    mapping = configparser.ConfigParser(
        interpolation=shellinterpolation.ExtendedShellInterpolation(str(mappingfile.resolve().parent)),
        default_section=DEFAULT_SECTION
    )
    mapping.read(mappingfile)
    
    if not mapping.has_section(META_SECTION):
        print(f"No {META_SECTION} section defined!")
        return None

    if not mapping.has_option(META_SECTION, 'templatepath') or not mapping.has_option(META_SECTION, 'outputpath'):
        print(f"Both templatepath and outputpath need to be defined in the {META_SECTION} section")
        return None
    
    templatepath = Path(mapping[META_SECTION]['templatepath'])
    outputpath = Path(mapping[META_SECTION]['outputpath'])
    
    if not (is_valid_path(templatepath) and is_valid_path(outputpath) and is_valid_path(mappingfile, is_file=True)):
        return None

    if templatepath.samefile(outputpath):
        print('Template and output paths cannot be the same!')
        return None

    return mapping


def main():
    parser = argparse.ArgumentParser(description='Generates configs from config templates')
    parser.add_argument('-d', '--diff', action='store_true', help='do not make any modifications to the filesystem, only print the diffs')
    parser.add_argument('-e', '--exit-on-change', action='store_true', help='exit on the first difference between current file and generated template file (return exit code 1 if has changes)')
    parser.add_argument('-n', '--no-modify', action='store_true', help='do not modify existing files but print the diffs, create new files and modify file modes')
    parser.add_argument('mappingfile', type=Path, metavar='MAPPINGFILE', help='a mapping file specifying strings to be replaced from templates')

    args = parser.parse_args()

    mapping = load_mapping(args.mappingfile)
    if not mapping:
        return
        
    templatepath = Path(mapping[META_SECTION]['templatepath'])
    outputpath = Path(mapping[META_SECTION]['outputpath'])

    main_section_proxy = get_main_section_proxy(mapping)

    generator = Generator(templatepath, outputpath, main_section_proxy, args.no_modify, args.diff, args.exit_on_change)
    generator.generate()


if __name__ == '__main__':
    main()
