import sys
import re
import os
from pathlib import Path
from difflib import unified_diff
from time import ctime
from os.path import getmtime


ATTR_PATTERN = re.compile(r'(?<=<<).+?(?=>>)')


class Generator:
    def __init__(self, templatepath, outputpath, main_section_proxy, no_modify, only_diffs, exit_on_change):
        self.templatepath = templatepath
        self.outputpath = outputpath
        self.main_section_proxy = main_section_proxy
        
        self.force_skip_modify = no_modify # prevent file modifications but allow other actions
        self.force_only_diffs = only_diffs # only print all diffs, no other actions
        self.force_exit_on_change = exit_on_change

        self.force_skip_all = False # on quit, switch to True to skip all processing after that
        

    def replace(self, file_path_templ, file_path_out):
        if self.force_skip_all:
            return f"\033[95m[FORCED SKIP]\033[0m {file_path_out}\n"

        # Make output path subdirectories
        if not file_path_out.parent.is_dir(): 
            os.makedirs(file_path_out.parent) 

        # Generate file from template
        with open(file_path_templ, "rt") as f_template:
            generated = self.replace_strings(f_template.read())
    
        # Read current output file (empty if file doesn't exist)
        with open(file_path_out, "a+t") as f_current:
            f_current.seek(0)
            current = f_current.read()

        # Write the config if files are different and the user accepts the diff between files
        if current != generated:
            if self.force_exit_on_change:
                sys.exit(1)
            
            if self.ask_accept_diff(current, generated, file_path_out):
                with open(file_path_out, "wt") as f_output:
                    f_output.write(generated)
                    status = "\033[92m[CREATED]\033[0m" if len(current) == 0 else "\033[91m[MODIFIED]\033[0m"
            else:
                status = "\033[95m[SKIPPED]\033[0m"
        else:
            status = "\033[94m[NO CHANGES]\033[0m"

        # Set same file mode as in template file
        if not self.force_only_diffs and os.stat(file_path_out).st_mode != (mode := os.stat(file_path_templ).st_mode):
            os.chmod(file_path_out, mode)
            status += f"\033[93m[SET MODE {mode}]\033[0m"

        return f"{status} {file_path_out}\n"


    def ask_accept_diff(self, current, generated, output_path):
        if not self.force_only_diffs and len(current) == 0:
            return True

        diff = unified_diff(
            current.split("\n"), 
            generated.split("\n"), 
            f"{output_path}   {ctime(getmtime(output_path))}",
            f"{output_path}   \033[96m(GENERATED)", 
            lineterm="", n=0
        )

        for line in list(diff):
            if line.startswith("+"):
                print(f"\033[92m{line}\033[0m")
            elif line.startswith("-"):
                print(f"\033[91m{line}\033[0m")
            else:
                print(line)

        if self.force_skip_modify or self.force_only_diffs:
            print()
            return False

        answer = input("\n\033[95mAccept this diff [Y/n/q]  (Yes/No/Quit): \033[0m\n").lower()
        if answer.startswith("q"):
            self.force_skip_all = True
            return False
    
        return len(answer) == 0 or answer.startswith("y") 
        


    def replace_strings(self, content):
        position = 0
        while (m := ATTR_PATTERN.search(content, position)):
            attr, *rest = m[0].split("|")

            if attr in self.main_section_proxy:
                value = self.main_section_proxy.get(attr, vars={str(i): self.main_section_proxy[val] for i, val in enumerate(rest)})
                content = content[0:m.start()-2] + value + content[m.end()+2:]
                position = m.start() + len(value)
            else:
                position = m.start() + 1

        return content
    

    def generate(self):
        statuses = []
    
        for root, dirs, files in os.walk(self.templatepath):
            # current os.walk path / template directory path
            template_dir_path = Path(root)

            # path to output directory
            output_dir_path = self.outputpath / template_dir_path.relative_to(self.templatepath) 

            for file_name in files:
                status = self.replace(template_dir_path / file_name, output_dir_path / file_name)
                statuses.append(status)

        if not self.force_only_diffs and not self.force_exit_on_change:
            sys.stdout.writelines(statuses)

        if self.force_skip_all:
            sys.exit(1)
