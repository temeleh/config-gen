# config-gen

Config generation script. Replaces strings (defined in a config/mapping file) from file templates and copies them to a new directory.

## Usage

config-gen.py [-h] MAPPINGFILE

The example mapping in the examples folder can be run (on linux) with ```./config-gen.py examples/mapping.ini``` in the project root folder

## Licence

Licensed under the GNU General Public License Version 3.0 (or later).
