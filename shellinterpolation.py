#!/usr/bin/env python3
from configparser import *
import re
import subprocess

class ExtendedShellInterpolation(Interpolation):
    _KEYCRE = re.compile(r"\$\{([^}]+)\}")

    def __init__(self, cwd):
        self.cwd = cwd

    def before_get(self, parser, section, option, value, defaults):
        L = []
        self._interpolate_some(parser, option, L, value, section, defaults, 1)
        return ''.join(L)

    def before_set(self, parser, section, option, value):
        tmp_value = value.replace('$$', '') # escaped dollar signs
        tmp_value = self._KEYCRE.sub('', tmp_value) # valid syntax
        if '$' in tmp_value:
            raise ValueError("invalid interpolation syntax in %r at "
                             "position %d" % (value, tmp_value.find('$')))
        return value

    def _interpolate_some(self, parser, option, accum, rest, section, map,
                          depth):
        rawval = parser.get(section, option, raw=True, fallback=rest)
        if depth > MAX_INTERPOLATION_DEPTH:
            raise InterpolationDepthError(option, section, rawval)
        while rest:
            p = rest.find("$")
            if p < 0:
                accum.append(rest)
                return
            if p > 0:
                accum.append(rest[:p])
                rest = rest[p:]
            # p is no longer used
            c = rest[1:2]
            if c == "$":
                accum.append("$")
                rest = rest[2:]
            elif (is_shell := (c == "!")) or c == "=":
                cmd = rest[2:]
                while "┊" in cmd:
                    cmd = self._parse_in_shell_var(cmd, section, option, map, parser, rawval, depth)

                if is_shell:
                    accum.append(subprocess.run(cmd, cwd=self.cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, 
                            encoding='UTF-8', shell=True).stdout)
                else:
                    accum.append(str(eval(cmd, {}))) # the eval is not completely safe and doesn't need to be

                break

            elif c == "{":
                m = self._KEYCRE.match(rest)
                if m is None:
                    raise InterpolationSyntaxError(option, section, "bad interpolation variable reference %r" % rest)

                path = m.group(1).split(':')
                rest = rest[m.end():]

                sect, opt, v = self._parse_inside(section, option, path, map, parser, rawval)

                if "$" in v:
                    self._interpolate_some(parser, opt, accum, v, sect, dict(parser.items(sect, raw=True)), depth + 1)
                else:
                    accum.append(v)
            else:
                raise InterpolationSyntaxError(
                    option, section,
                    "'$' must be followed by '$' or '{', "
                    "found: %r" % (rest,))
    

    def _parse_inside(self, section, option, path, map, parser, rawval):
        sect = section
        opt = option
        try:
            if len(path) == 1:
                opt = parser.optionxform(path[0])
                v = map[opt]
            elif len(path) == 2:
                sect = path[0]
                opt = parser.optionxform(path[1])
                v = parser.get(sect, opt, raw=True)
            else:
                raise InterpolationSyntaxError(
                    option, section,
                    "More than one ':' found: %r" % (rest,))
        except (KeyError, NoSectionError, NoOptionError):
            raise InterpolationMissingOptionError(option, section, rawval, ":".join(path)) from None

        return sect, opt, v

    def _parse_in_shell_var(self, cmd, section, option, map, parser, rawval, depth):
        opt = cmd[cmd.index("┊") + 1:]
        opt = opt[:opt.index("┊")]

        path = opt.split(':')
        sect, opt, val = self._parse_inside(section, option, path, map, parser, rawval)

        R = []
        self._interpolate_some(parser, opt, R, val, sect, dict(parser.items(sect, raw=True)), depth + 1)

        val = ''.join(R)
        opt = opt + "┊"
        return cmd.replace("┊" + opt, val, 1).replace("┊" + sect + ":" + opt, val)
